﻿using Jobble.Core;
using Jobble.ViewModel;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace Jobble.View
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TinderPage : ContentPage
    {
        public TinderPage()
        {
            InitializeComponent();
            BindingContext = new TinderPageViewModel();

            SwipeCardView.Dragging += OnDragging;
        }
        private async void OnDislikeClicked(object sender, EventArgs e)
        {
           await SwipeCardView.InvokeSwipe(SwipeCardDirection.Left);
        }

       
        private async void OnLikeClicked(object sender, EventArgs e)
        {
           
            try
            {
                // Use default vibration length
               // Vibration.Vibrate();
                await SwipeCardView.InvokeSwipe(SwipeCardDirection.Right);
               
                // Or use specified time
                var duration = TimeSpan.FromSeconds(500);
                Vibration.Vibrate(duration);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

        private void OnDragging(object sender, DraggingCardEventArgs e)
        {
            var view = (Xamarin.Forms.View)sender;
            var nopeFrame = view.FindByName<Frame>("NopeFrame");
            var likeFrame = view.FindByName<Frame>("LikeFrame");
            //var superLikeFrame = view.FindByName<Frame>("SuperLikeFrame");
            var threshold = (this.BindingContext as TinderPageViewModel).Threshold;

            var draggedXPercent = e.DistanceDraggedX / threshold;

            var draggedYPercent = e.DistanceDraggedY / threshold;

            switch (e.Position)
            {
                case DraggingCardPosition.Start:
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    //superLikeFrame.Opacity = 0;                    
                    nopeButton.Scale = 1;
                    likeButton.Scale = 1;
                    //superLikeButton.Scale = 1;
                    break;
                case DraggingCardPosition.UnderThreshold:
                    if (e.Direction == SwipeCardDirection.Left)
                    {
                        nopeFrame.Opacity = (-1) * draggedXPercent;
                        nopeButton.Scale = 1 + draggedXPercent / 2;
                        //superLikeFrame.Opacity = 0;
                        //superLikeButton.Scale = 1;

                    }
                    else if (e.Direction == SwipeCardDirection.Right)
                    {
                        likeFrame.Opacity = draggedXPercent;
                        likeButton.Scale = 1 - draggedXPercent / 2;
                       // superLikeFrame.Opacity = 0;
                        //superLikeButton.Scale = 1;

                    }
                    else if (e.Direction == SwipeCardDirection.Up)
                    {
                        nopeFrame.Opacity = 0;
                        likeFrame.Opacity = 0;
                        nopeButton.Scale = 1;
                        likeButton.Scale = 1;
                       // superLikeFrame.Opacity = (-1) * draggedYPercent;
                        //superLikeButton.Scale = 1 + draggedYPercent / 2;
                    }
                    break;
                case DraggingCardPosition.OverThreshold:
                    if (e.Direction == SwipeCardDirection.Left)
                    {
                        nopeFrame.Opacity = 1;
                       // superLikeFrame.Opacity = 0;
                    }
                    else if (e.Direction == SwipeCardDirection.Right)
                    {
                        likeFrame.Opacity = 1;
                        //superLikeFrame.Opacity = 0;
                    }
                    else if (e.Direction == SwipeCardDirection.Up)
                    {
                        nopeFrame.Opacity = 0;
                        likeFrame.Opacity = 0;
                       // superLikeFrame.Opacity = 1;
                    }
                    break;
                case DraggingCardPosition.FinishedUnderThreshold:
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    //superLikeFrame.Opacity = 0;
                    nopeButton.Scale = 1;
                    likeButton.Scale = 1;
                    //superLikeButton.Scale = 1;
                    break;
                case DraggingCardPosition.FinishedOverThreshold:
                    nopeFrame.Opacity = 0;
                    likeFrame.Opacity = 0;
                    //superLikeFrame.Opacity = 0;
                    nopeButton.Scale = 1;
                    likeButton.Scale = 1;
                    //superLikeButton.Scale = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}