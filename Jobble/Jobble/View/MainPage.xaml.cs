﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Jobble.Services;
using Jobble.Model;
using System.Collections.Generic;
using System.Linq;
using Jobble.ViewModel;
using Jobble.View;

namespace Jobble
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public IList<Candidate> ListCandidates { get; set; }      
        RestService _rs;         
        public MainPage()
        {
            InitializeComponent();
            // Navigate to pages
            //BindingContext = new MainPageViewModel(Navigation);
            // IList Candidates instance 
            ListCandidates = new List<Candidate>();         
            // Rest Services
            _rs = new RestService();
           
            // call method for display all data from WebAPI
            GetAllCandidates();
        }

        // Display all Data from Web API
        private async  void GetAllCandidates()
        {
            // Get List of candidate data 
            ListCandidates = await _rs.GetCandidateDataAsync(GenerateRequestUri(Constant.WebApiEndPoints));
            OnPropertyChanged(nameof(ListCandidates));
            BindingContext = this;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {            
            string searchWord = searchText.Text;
            if (!string.IsNullOrEmpty(searchWord) && (ListCandidates != null))
            {
                var results = ListCandidates.Where(x => x.Technology[0].Name.ToLower() == searchWord.ToLower()).ToList();
                ListCandidates = results;
                OnPropertyChanged(nameof(ListCandidates));
                BindingContext = this;
            }            
        }
       public string GenerateRequestUri(string endpoint)
        {
            return endpoint;            
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Candidate selectedItem = e.SelectedItem as Candidate;
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Candidate tappedItem = e.Item as Candidate;
        }

        private void Button_Clicked_Refresh(object sender, EventArgs e)
        {
            // Get all candidates
            GetAllCandidates();
        }

        private void Button_Clicked_TinderPage(object sender, EventArgs e)
        {
            TinderPage page = new TinderPage();
            Navigation.PushAsync(page);
        }
    }
}
