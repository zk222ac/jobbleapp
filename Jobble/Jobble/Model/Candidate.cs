﻿using Newtonsoft.Json;

namespace Jobble.Model
{
    public class Candidate
    {
        [JsonProperty("name")]
        public FullName Name { get; set; }
        

        [JsonProperty("technologies")]
        public Technologies[] Technology { get; set; }

        [JsonProperty("about")]
        public string About { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }

        public override string ToString()
        {
            // return $"{Name.First}-{Name.Last}-{Technology}-{About}- {Picture}";
            return $"{Name.First}-{Name.Last}";
        }


    }
    public class FullName
    {
        [JsonProperty("first")]
        public string First { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }

    }

    public class Technologies
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("experianceYears")]
        public int ExperienceYears { get; set; }

    }
}
