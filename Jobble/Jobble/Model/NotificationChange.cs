﻿using System.ComponentModel;

namespace Jobble.Model
{
   public class NotificationChange : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged (string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
