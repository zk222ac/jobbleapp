﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Jobble.Model;
using Newtonsoft.Json;

namespace Jobble.Services
{
    public class RestService
    {
        HttpClient _client;

        public RestService()
        {
            _client = new HttpClient();
        }

        public async Task<IList<Candidate>> GetCandidateDataAsync(string uri)
        {
            IList<Candidate> CandidateData = null;
            try
            {
                HttpResponseMessage response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    CandidateData = JsonConvert.DeserializeObject<IList<Candidate>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return CandidateData;
        }
    }
}
